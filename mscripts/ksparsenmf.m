function [U,V,errors] = ksparsenmf(M,k,U0,V0)
%KSPARSENMF Compute the k-sparse NMF of M, that is, find U and V such that
%M~=UV and V is column-wise k-sparse

% Parameters
maxiter = 50;
iter_hals = 100;

% Init
U = U0;
V = V0;

% Main loop
it = 1;
errors = zeros(maxiter,1);
times = zeros(maxiter,1);
tic;
while it < maxiter
    fprintf('Iteration %d out of %d\n',it,maxiter);
    % Update U
    U = nnlsHALSupdt(M',V',U',iter_hals)';
    % Update V
    [V,~,~,~] = arborescentMex(M,U,k,V);
    % Compute relative error
    errors(it) = norm(M-U*V,'fro')/norm(M,'fro');
    % Check time
    times(it) = toc;
    % Increment
    it = it+1;
end

% Draw plot
%TODO

end
