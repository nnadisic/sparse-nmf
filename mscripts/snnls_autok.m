% Run Sparse NNLS with arborescent and detects the "real k"
% by finding the critical point of the Pareto Front returned by
% arborescent, that is, the lowest k' for which the relative-error
% gap between k' and k'+1 is smaller than a given epsilon

function [x, detectedk] = snnls_autok (A,b,targetk,eps)

    [~,n] = size(A);

    % Run arborescent
    [~,~,arbosols] = arboNNLSMex(A,b,targetk);

    % Compute error of the solution of every k'
    kprime_errors = [];
    it = 0;
    for i=targetk:n
        it = it +1;
        kprime_errors = [kprime_errors norm(A*arbosols(:,it)-b)];
    end
    
    % Relative errors
    relerrs = kprime_errors/norm(b);

    % Compute gaps between k' solutions' relative errors
    gaps = relerrs(2:n) - relerrs(1:n-1);

    % The "real" k
    criticalpoint = find(gaps > eps, 1);
    detectedk = n + 1 - criticalpoint;

    % Return the corresponding x
    x = arbosols(:,criticalpoint);
end