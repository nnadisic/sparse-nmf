#include "../main/snmf.h"
#include "mexutils.h"

/* To compile in matlab, when in the root folder, type:
mex src/mex/arborescentMex.cc src/mex/mexutils.cc src/main/snmf.cc src/main/bab.cc src/utils/*.cc -lgsl -lcblas -latlas
*/

void
mexFunction (int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[])
{
  /* Convert input to gsl_matrix */
  gsl_matrix *mat_m = mxmat_to_gslmat (prhs[0]);
  gsl_matrix *mat_u = mxmat_to_gslmat (prhs[1]);
  uint n = mat_m->size2;
  uint r = mat_u->size2;

  /* Get parameters */
  double *params = mxGetPr (prhs[2]);
  uint sparsity = (uint) params[0];

  /* Security check, k cannot be greater than r */
  if (sparsity > r)
    sparsity = r;

  /* If initial V is given, take it, if not, init all-zero */
  gsl_matrix *mat_v;
  if (nrhs > 3)
    mat_v = mxmat_to_gslmat (prhs[3]);
  else
    mat_v = gsl_matrix_calloc (r, n);

  /* Initialize outputs */
  double time = 0, error = 0;
  gsl_vector *v_nne = gsl_vector_alloc (n); // nb of nodes explored (per col)

  /* Call solver */
  snmf (mat_m, mat_u, sparsity, mat_v, &time, &error, v_nne);

  /* Convert outputs and prepare the return */
  plhs[0] = gslmat_to_mxmat (mat_v);
  plhs[1] = mxCreateDoubleScalar(error);
  plhs[2] = mxCreateDoubleScalar(time);
  plhs[3] = gslvec_to_mxmat (v_nne);

  /* Clean memory */
  gsl_matrix_free (mat_m);
  gsl_matrix_free (mat_u);
  gsl_matrix_free (mat_v);
  return;
}
