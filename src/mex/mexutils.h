// mexutils.h

#include <gsl/gsl_matrix.h>
#include "mex.h"

/* Convert a mxArray to a gsl_matrix */
gsl_matrix *mxmat_to_gslmat (const mxArray * mxmat);

/* Convert a mxArray to a gsl_matrix */
gsl_vector *mxmat_to_gslvec (const mxArray * mxmat);

/* Convert a gsl_matrix to a mxArray */
mxArray *gslmat_to_mxmat (const gsl_matrix * gslmat);

/* Convert a gsl_vector to a mxArray */
mxArray *gslvec_to_mxmat (const gsl_vector * gslvec);

/* Print a gsl_matrix in Matlab with mexPrintf (debugging purpose) */
void mexprint_gslmat (const gsl_matrix * m);

/* Print a gsl_vector in Matlab with mexPrintf (debugging purpose) */
void mexprint_gslvect (const gsl_vector * v);
