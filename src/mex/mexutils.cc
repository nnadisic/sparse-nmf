#include "mexutils.h"


/* Convert a mxArray to a gsl_matrix */
gsl_matrix *
mxmat_to_gslmat (const mxArray * mxmat)
{
  const mwSize *dims = mxGetDimensions (mxmat);
  int nb_row = (int) dims[0];
  int nb_col = (int) dims[1];
  double *rawmat = mxGetPr (mxmat);

  gsl_matrix *m = gsl_matrix_alloc (nb_row, nb_col);
  for (int i = 0; i < nb_row; ++i)
    for (int j = 0; j < nb_col; ++j)
      gsl_matrix_set (m, i, j, rawmat[i + j * nb_row]);

  return m;
}


/* Convert a mxArray to a gsl_matrix */
gsl_vector *
mxmat_to_gslvec (const mxArray * mxmat)
{
  const mwSize *dims = mxGetDimensions (mxmat);
  int size = (int) dims[0];
  double *rawvec = mxGetPr (mxmat);

  gsl_vector *m = gsl_vector_alloc (size);
  for (int i = 0; i < size; ++i)
    gsl_vector_set (m, i, rawvec[i]);

  return m;
}


/* Convert a gsl_matrix to a mxArray */
mxArray *
gslmat_to_mxmat (const gsl_matrix * gslmat)
{
  int nb_row = gslmat->size1;
  int nb_col = gslmat->size2;

  mxArray *m = mxCreateDoubleMatrix (nb_row, nb_col, mxREAL);
  double *pm = mxGetPr (m);
  for (int i = 0; i < nb_row; ++i)
    for (int j = 0; j < nb_col; ++j)
      pm[i + j * nb_row] = gsl_matrix_get (gslmat, i, j);

  return m;
}


/* Convert a gsl_vector to a mxArray */
mxArray *
gslvec_to_mxmat (const gsl_vector * gslvec)
{
  int size = gslvec->size;

  mxArray *m = mxCreateDoubleMatrix (size, 1, mxREAL);
  double *pm = mxGetPr (m);
  for (int i = 0; i < size; ++i)
    pm[i] = gsl_vector_get (gslvec, i);

  return m;
}

/* Print a gsl_matrix in Matlab with mexPrintf (debugging purpose) */
void
mexprint_gslmat (const gsl_matrix * m)
{
  for (uint i = 0; i < m->size1; ++i)
    {
      for (uint j = 0; j < m->size2; ++j)
	mexPrintf ("%g ", gsl_matrix_get (m, i, j));
      mexPrintf ("\n");
    }
}

/* Print a gsl_vector in Matlab with mexPrintf (debugging purpose) */
void
mexprint_gslvect (const gsl_vector * v)
{
  for (uint i = 0; i < v->size; ++i)
    mexPrintf ("%g ", gsl_vector_get (v, i));
  mexPrintf ("\n");
}
