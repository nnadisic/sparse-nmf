#include "../main/bab.h"
#include "mexutils.h"

/* To compile in matlab, when in the root folder, type:
mex src/mex/arboNNLSMex.cc src/mex/mexutils.cc src/main/bab.cc src/utils/*.cc -lgsl -lcblas -latlas

To use in Matlab:
[x,nne] = arboNNLSMex(A,b,k)
*/

void
mexFunction (int nlhs, mxArray * plhs[], int nrhs, const mxArray * prhs[])
{
  /* Convert input to gsl_matrix and gsl_vector */
  gsl_matrix *a = mxmat_to_gslmat (prhs[0]);
  gsl_vector *b = mxmat_to_gslvec (prhs[1]);
  uint m = a->size1;
  uint n = a->size2;

  if (b->size != m)
    mexPrintf ("Error: Check sizes of A and b\n");

  /* Get parameters */
  double *params = mxGetPr (prhs[2]);
  uint k = (uint) params[0]; // sparsity

  /* If initial x is given, take it, if not, init all-zero */
  gsl_vector *x;
  if (nrhs > 3)
    x = mxmat_to_gslvec (prhs[3]);
  else
    x = gsl_vector_calloc (n);

  /* Initialize outputs */
  int nne = 0; // nb of nodes explored (per col)
  gsl_matrix *sols = gsl_matrix_alloc (n, n-k+1); // solutions, one foreach k'

  /* Compute A'A once and for all */
  gsl_matrix *ata = gsl_matrix_alloc (n, n);
  gsl_blas_dgemm (CblasTrans, CblasNoTrans, 1, a, a, 0, ata);

  /* Security, k cannot be greater than n */
  if (k > n)
    k = n;

  /* Call solver */
  solve_snnls (a, ata, b, k, x, &nne, sols);

  /* Convert outputs and prepare the return */
  plhs[0] = gslvec_to_mxmat (x);
  plhs[1] = mxCreateDoubleScalar (nne);
  plhs[2] = gslmat_to_mxmat (sols);

  /* Clean memory */
  gsl_matrix_free (a);
  gsl_matrix_free (ata);
  gsl_vector_free (b);
  gsl_vector_free (x);
  gsl_matrix_free (sols);
  return;
}
