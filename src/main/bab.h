// bab.h

#include <stack>
#include <limits>
#include <vector>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_sort.h>
#include <gsl/gsl_sort_vector.h>
#include <gsl/gsl_permutation.h>
#include <gsl/gsl_permute_vector.h>
#include <gsl/gsl_permute_matrix.h>

#include "../utils/nnls.h"


struct node
{
  // Attributes
  gsl_vector * parent_sol;
  std::vector<uint> nonzero_elts;
  uint biggest_zeroed_elt;
  // Constructors
  node () : parent_sol(0), nonzero_elts(0), biggest_zeroed_elt(0) {};
  node (gsl_vector * x, std::vector<uint> nze, uint bze) :
    parent_sol(x), nonzero_elts(nze), biggest_zeroed_elt(bze) {};
};

// Actual branch and bound function
double snnls_bab (const gsl_matrix *a, const gsl_vector *b,
		  const gsl_matrix *ata, const gsl_vector *atb, const uint k,
		  gsl_vector *x, int *nne, gsl_matrix * sols);

// Interface for SNNLS solver
double solve_snnls (const gsl_matrix *a, const gsl_matrix *ata,
		    const gsl_vector *b, const uint k, gsl_vector *x,
		    int *nne, gsl_matrix * sols);
