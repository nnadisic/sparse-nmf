#include "bab.h"

// snnls_bab -- here we branch and we bound
// x needs to be intialized with the desired x0 value
// At return, x contains the final solution
double
snnls_bab (const gsl_matrix * a, const gsl_vector * b,
	   const gsl_matrix * ata, const gsl_vector * atb,
	   const uint k, gsl_vector * x, int *nne, gsl_matrix * kprime_sols)
{
  // Constants
  const uint xsize = x->size;

  // Init pool
  std::stack<node> pool;
  std::vector<uint> all_elts;
  for (uint i = 0; i < xsize; ++i)
    all_elts.push_back (i);
  struct node root_node = { x, all_elts, 0 };

  pool.push (root_node);

  // Init best_x and best_error
  gsl_vector *best_x = gsl_vector_alloc (xsize);
  gsl_vector_memcpy (best_x, x);
  double best_error = std::numeric_limits<double>::max();

  // Init working variables
  struct node cur_node;
  gsl_vector *cur_x = gsl_vector_alloc (xsize);
  double cur_error;
  gsl_vector *kprime_besterr = gsl_vector_alloc (xsize);
  gsl_vector_set_all (kprime_besterr, best_error);

  // Init number of nodes explored
  *nne = 0;

  // While pool is not empty
  while (!pool.empty ())
    {
      // Select cur_node
      cur_node = pool.top ();
      pool.pop ();
      (*nne)++; // one more node explored
      uint kprime = cur_node.nonzero_elts.size ();
      uint depth = xsize - kprime;
      // Compute NNLS, find cur_error and update x
      gsl_vector_memcpy (cur_x, cur_node.parent_sol);
      std::vector<uint> cur_nze = cur_node.nonzero_elts;
      cur_error = reduced_nnls (a, b, ata, atb, cur_nze, cur_x);

      // Test (and possibly update) the best sol for current k'
      if (cur_error < gsl_vector_get (kprime_besterr, depth))
	{
	  gsl_matrix_set_col (kprime_sols, depth, cur_x);
	  gsl_vector_set (kprime_besterr, depth, cur_error);
	}

      // If the error is above the upper bound
      if (cur_error > best_error)
	{
	  // Prune (do nothing)
	}
      // Else, explore the subspace
      else
	{
	  std::vector<uint> nze = cur_node.nonzero_elts;
	  // If the desired sparsity k is not reached
	  if (kprime > k)
	    {
	      uint bze = cur_node.biggest_zeroed_elt;
	      // Branch: new node for every nonzero element,
	      // every child node will have one new elem put to 0
	      for (int i = nze.size () - 1; i >= 0; --i)
		{
		  // Avoid identical nodes in different branches
		  if (bze > nze.at (i))
		    break;
		  // Child node's nonzero elements
		  std::vector < uint > child_nze (nze);
		  child_nze.erase (child_nze.begin () + i);
		  // Child node's biggest zeroed element
		  uint child_bze =
		    (bze > (uint) nze.at (i) ? bze : nze.at (i));
		  // Create new node and push it to the pool
		  struct node child_node = { cur_x, child_nze, child_bze };
		  pool.push (child_node);
		}
	    }
	  // Else, we have reached the desired sparsity k
	  else
	    {
	      // If cur_error < best_error then updt best x and best error
	      if (cur_error < best_error)
		{
		  gsl_vector_memcpy (best_x, cur_x);
		  best_error = cur_error;
		  // printf("new best error: %g\n", best_error);
		}
	    }
	}
    }

  // Final solution x is returned by the x pointer in parameter
  gsl_vector_memcpy (x, best_x);
  gsl_vector_free (best_x);
  gsl_vector_free (cur_x);
  gsl_vector_free (kprime_besterr);
  return best_error;
}

// Interface for SNLLS: prepare the input, sort x (and A), and run
double
solve_snnls (const gsl_matrix * a, const gsl_matrix * ata,
	     const gsl_vector * b, const uint k, gsl_vector * x,
	     int *nne, gsl_matrix * sols)
{
  // Init A'b
  gsl_vector *atb = gsl_vector_alloc (a->size2);
  gsl_blas_dgemv (CblasTrans, 1, a, b, 0, atb);

  // If initial x is not provided, pick the solution to unconstrained NNLS
  if (gsl_vector_isnull (x))
    {
      // Random vector
      for (uint i = 0; i < x->size; i++)
	gsl_vector_set (x, i, (double) rand () / RAND_MAX);
      // Apply NNLS to find an initial x
      nnls (ata, atb, x);
    }

  // Sort elements of initial x (and corresp col of A, and corresp col
  // and lines of A'A) by ascending order
  gsl_permutation *permu = gsl_permutation_alloc (x->size);
  gsl_sort_vector_index (permu, x);
  gsl_permute_vector (permu, x);
  gsl_matrix *sorted_a = gsl_matrix_alloc (a->size1, a->size2);
  gsl_matrix_memcpy (sorted_a, a);
  gsl_permute_matrix (permu, sorted_a);
  gsl_matrix *sorted_ata = gsl_matrix_alloc (ata->size1, ata->size2);
  gsl_matrix_memcpy (sorted_ata, ata);
  gsl_permute_matrix (permu, sorted_ata);
  gsl_matrix_transpose (sorted_ata);
  gsl_permute_matrix (permu, sorted_ata);
  gsl_matrix_transpose (sorted_ata);

  // Compute A'b once and for all
  gsl_blas_dgemv (CblasTrans, 1, sorted_a, b, 0, atb);

  // Main loop
  double final_error = snnls_bab (sorted_a, b, sorted_ata, atb, k, x,
				  nne, sols);

  // Sort again x in the right way, for good output
  gsl_permutation *permu_inv = gsl_permutation_alloc (x->size);
  gsl_permutation_inverse (permu_inv, permu);
  gsl_permute_vector (permu_inv, x);
  // Also sort sols
  gsl_matrix *sols_t = gsl_matrix_alloc (sols->size2, sols->size1);
  gsl_matrix_transpose_memcpy (sols_t, sols);
  gsl_permute_matrix (permu_inv, sols_t);
  gsl_matrix_transpose_memcpy (sols, sols_t);

  gsl_vector_free (atb);
  gsl_permutation_free (permu);
  gsl_permutation_free (permu_inv);
  gsl_matrix_free (sorted_a);
  gsl_matrix_free (sorted_ata);
  gsl_matrix_free (sols_t);
  return final_error;
}
