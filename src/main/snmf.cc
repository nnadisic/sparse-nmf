#include "snmf.h"

// The actual SNMF main loop: find V s.t. M = UV with M and U known
// and V column-wise k-sparse
void
snmf (gsl_matrix * mat_m, const gsl_matrix * mat_u, const uint k,
      gsl_matrix * mat_v, double *time, double *error, gsl_vector * v_nne)
{
  // Init
  uint n = mat_m->size2;
  uint r = mat_u->size2;

  // Begin to count time
  clock_t begin_time = clock ();

  // Compute U'U once and for all
  gsl_matrix *utu = gsl_matrix_alloc (r, r);
  gsl_blas_dgemm (CblasTrans, CblasNoTrans, 1, mat_u, mat_u, 0, utu);

  // TMP create empty matrix to pass to snnls
  // TODO: take a decision and fix that
  gsl_matrix *sols = gsl_matrix_alloc (r, r - k + 1);

  // Loop: do NNLS on every column of V
  // #pragma omp parallel for // option to parallelize loop
  for (uint i = 0; i < n; ++i)
    {
      // if (i % 100 == 0)
      // 	fprintf (stderr, "%u / %u\n", i, n);
      gsl_vector_view x = gsl_matrix_column (mat_v, i);
      gsl_vector_view b = gsl_matrix_column (mat_m, i);
      int nne; // Number of nodes explored in BaB
      solve_snnls (mat_u, utu, &b.vector, k, &x.vector, &nne, sols);
      gsl_vector_set (v_nne, i, nne);
      // printf("nne %i\n", nne);
    }

  // Total time
  *time = ((double) (clock () - begin_time) / CLOCKS_PER_SEC);

  // Final reconstruction error
  gsl_matrix *reconstr = gsl_matrix_alloc (mat_m->size1, mat_m->size2);
  gsl_matrix_memcpy (reconstr, mat_m);
  gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1, mat_u, mat_v, -1, reconstr);
  *error = frobenius_norm (reconstr);

  gsl_matrix_free (utu);
  gsl_matrix_free (reconstr);
}
