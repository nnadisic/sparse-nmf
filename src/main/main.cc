#include <string>
#include <iostream>
#include <boost/program_options.hpp>

#include "snmf.h"
#include "../utils/operations.h"


namespace po = boost::program_options;

// Main: manage i/o
int
main (int argc, char *argv[])
{
  // Init input variables
  std::string mat_m_file, mat_u_file, mat_v_file, out_v_file;
  uint sparsity;

  // Define options and read input
  po::options_description opt("Parameters");
  opt.add_options()
    ("mat-m,m", po::value<std::string>(&mat_m_file)->required(),
     "input matrix M")
    ("mat-u,u", po::value<std::string>(&mat_u_file)->required(),
     "input matrix U")
    ("mat-v,v", po::value<std::string>(&mat_v_file),
     "(optional) initial matrix V")
    ("sparsity,k", po::value<uint>(&sparsity)->required(),
     "level of column-wise sparsity desired (number of non-zero values)")
    ("output-v,o", po::value<std::string>(&out_v_file),
     "(optional) file to write output matrix V;\nfor output in terminal use -o /dev/stdout");
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, opt), vm);

  // If the user forgets parameters, print the help
  try
    {
      po::notify(vm);
    }
  catch (boost::program_options::error& e)
    {
      std::cout << opt << std::endl;
      return 1;
    }

  // Build matrices
  gsl_matrix *mat_m = read_matrix_from_txt (mat_m_file.c_str()); // alloc
  gsl_matrix *mat_u = read_matrix_from_txt (mat_u_file.c_str()); // alloc

  uint n = mat_m->size2;
  uint r = mat_u->size2;

  // Security check, k cannot be greater than r
  if (sparsity > r)
    sparsity = r;

  // If initial V is given, take it, if not, init all-zero
  gsl_matrix *mat_v;
  if (!mat_v_file.empty())
    mat_v = read_matrix_from_txt (mat_v_file.c_str());
  else
    mat_v = gsl_matrix_calloc (r, n);

  // Normalize m if needed
  // normalize_cols (mat_m);

  // Init outputs and call snmf
  gsl_vector *v_nne = gsl_vector_alloc (n); // nb of nodes explored (per col)
  double time = 0, error = 0;
  snmf (mat_m, mat_u, sparsity, mat_v, &time, &error, v_nne);

  // Output
  printf("Time: %f\nError: %f\n", time, error);
  if (!out_v_file.empty())
    print_gslmat_to_txt (mat_v, out_v_file.c_str());

  // TODO: output v_nne

  // printf("Resulting matrix V\n");
  // print_gslmat (mat_v);

  // Clean memory
  gsl_matrix_free (mat_m);
  gsl_matrix_free (mat_u);
  gsl_matrix_free (mat_v);
  return 0;
}
