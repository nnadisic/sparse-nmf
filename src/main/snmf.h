// snmf.h

#include <ctime>
#include "../utils/files_io.h"
#include "../utils/operations.h"
#include "bab.h"

void snmf (gsl_matrix *mat_m, const gsl_matrix *mat_u, const uint k,
	   gsl_matrix *mat_v, double *time, double *error, gsl_vector *nne);
