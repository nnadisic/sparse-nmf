// files_io.cc

#include "files_io.h"

// Create and return a gsl_matrix by reading the given text file
gsl_matrix *
read_matrix_from_txt (const char *filename)
{
  FILE *file = fopen (filename, "r");
  int m, n;
  int ret = fscanf (file, "%i %i\n", &m, &n);
  if (ret != 2)
    printf ("Failed to read %s\n", filename);
  gsl_matrix *mat = gsl_matrix_alloc (m, n);
  gsl_matrix_fscanf (file, mat);
  return mat;
}

// Create and return a gsl_vector by reading the given text file
gsl_vector *
read_vector_from_txt (const char *filename)
{
  FILE *file = fopen (filename, "r");
  int size;
  int ret = fscanf (file, "%i\n", &size);
  if (ret != 2)
    printf ("Failed to read %s\n", filename);
  gsl_vector *vect = gsl_vector_alloc (size);
  gsl_vector_fscanf (file, vect);
  return vect;
}

// Print a gsl_matrix into a txt file
void
print_gslmat_to_txt (const gsl_matrix * m, const char *filename)
{
  FILE *outfile = fopen (filename, "w");
  for (uint i = 0; i < m->size1; ++i)
    {
      for (uint j = 0; j < m->size2; ++j)
	fprintf (outfile, "%g ", gsl_matrix_get (m, i, j));
      fprintf (outfile, "\n");
    }
  fclose (outfile);
}

// Print a gsl_matrix in a human-readable way
void
print_gslmat (const gsl_matrix * m)
{
  for (uint i = 0; i < m->size1; ++i)
    {
      for (uint j = 0; j < m->size2; ++j)
	printf ("%g ", gsl_matrix_get (m, i, j));
      printf ("\n");
    }
}

// Print a gsl_vector in a human-readable way
void
print_gslvect (const gsl_vector * v)
{
  for (uint i = 0; i < v->size; ++i)
    printf ("%g ", gsl_vector_get (v, i));
  printf ("\n");
}
