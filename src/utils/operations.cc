#include "operations.h"

double
frobenius_norm (gsl_matrix * m)
{
  double norm = 0.0;
  for (uint i = 0; i < m->size1; ++i)
    for (uint j = 0; j < m->size2; ++j)
      {
	double value = gsl_matrix_get (m, i, j);
	norm += value * value;
      }
  return sqrt (norm);
}


void
normalize_cols (gsl_matrix * m)
{
  for (uint i = 0; i < m->size2; ++i)
    {
      gsl_vector_view col = gsl_matrix_column (m, i);
      double d = gsl_blas_dnrm2 (&col.vector);
      gsl_blas_dscal (1 / d, &col.vector);
    }
}
