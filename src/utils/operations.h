// operations.h

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <math.h>

// Returns the Frobenius norm of input matrix
double frobenius_norm (gsl_matrix * m);

// Normalize to 1 all the columns of input matrix
void normalize_cols (gsl_matrix * m);
