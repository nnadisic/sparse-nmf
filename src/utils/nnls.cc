#include "nnls.h"

// Adapted from the paper "Toward Faster NMF [...]", Kim and Park 2008
// Active set method

void
nnls (const gsl_matrix * ata, const gsl_vector * atb, gsl_vector * x)
{
  // Init variables
  const uint n = ata->size1;
  uint t = n + 1;
  uint p = 3;
  uint notgood;
  gsl_vector *sol = gsl_vector_alloc (n);
  gsl_vector_memcpy (sol, x); // init sol with parent solution
  gsl_vector *grad = gsl_vector_calloc (n);

  std::vector<uint> f; // passive set: unconstrained elements
  std::vector<uint> nonf; // active set: elements constrained to 0

  // Avoid initialization limit case
  if (n == 1)
    f.push_back (0);
  else
    {
      // Init the sets with provided initial solution
      for (uint i = 0; i < n; ++i)
	{
	  if (gsl_vector_get (sol, i) == 0)
	    nonf.push_back (i);
	  else
	    f.push_back (i);
	}
      // Safety to avoid segfault if one set is empty
      if (f.empty ())
	{
	  f.push_back (0);
	  nonf.erase (nonf.begin ());
	}
      if (nonf.empty ())
	{
	  nonf.push_back (0);
	  f.erase (f.begin ());
	}
    }

  boost::dynamic_bitset<> h1(n), h1c(n), h2(n), h2c(n);

  // Init GSL least-squares workspace
  gsl_multifit_linear_workspace *workspace = gsl_multifit_linear_alloc (n, n);
  double chisq;

  // NNLS Loop /////////////////////////////////////////////////////////////
  uint it = 0;
  do
    {
      ++it;
      // Create A'A(F) and A' b(F)
      uint fsize = f.size ();
      gsl_matrix *ata_f = gsl_matrix_alloc (fsize, fsize);
      gsl_vector *atb_f = gsl_vector_alloc (fsize);
      for (uint i = 0; i < fsize; ++i)
	{
	  for (uint j = 0; j < fsize; ++j)
	    gsl_matrix_set (ata_f, i, j,
			    gsl_matrix_get (ata, f.at (i), f.at (j)));
	  gsl_vector_set (atb_f, i, gsl_vector_get (atb, f.at (i)));
	}

      // Compute solution vector with least-squares
      gsl_vector *sol_f = gsl_vector_alloc (fsize);
      gsl_matrix *cov = gsl_matrix_alloc (fsize, fsize);
      gsl_multifit_linear (ata_f, atb_f, sol_f, cov, &chisq, workspace);
      gsl_vector_set_zero (sol);
      for (uint i = 0; i < fsize; ++i)
	gsl_vector_set (sol, f.at (i), gsl_vector_get (sol_f, i));

      // Create A'A(~F) and A' b(~F)
      uint nfsize = nonf.size ();
      gsl_matrix *ata_nf = gsl_matrix_alloc (nfsize, fsize);
      gsl_vector *atb_nf = gsl_vector_alloc (nfsize);
      for (uint i = 0; i < nfsize; ++i)
	{
	  for (uint j = 0; j < fsize; ++j)
	    gsl_matrix_set (ata_nf, i, j,
			    gsl_matrix_get (ata, nonf.at (i), f.at (j)));
	  gsl_vector_set (atb_nf, i, gsl_vector_get (atb, nonf.at (i)));
	}

      // Compute gradient
      gsl_vector *grad_nf = gsl_vector_alloc (nfsize);
      gsl_vector_memcpy (grad_nf, atb_nf);
      // grad(~F) = A'A(~F) * sol(F) - A'b(~F)
      gsl_blas_dgemv (CblasNoTrans, 1, ata_nf, sol_f, -1, grad_nf);
      gsl_vector_set_zero (grad);
      for (uint i = 0; i < nfsize; ++i)
	gsl_vector_set (grad, nonf.at (i), gsl_vector_get (grad_nf, i));

      // Suppress numeric noise
      for (uint i = 0; i < n; ++i)
	{
	  double *val = gsl_vector_ptr (sol, i);
	  *val = (abs (*val) < 0.0001) ? 0 : *val;
	}

      // Compute H1 and H2
      h1.reset ();
    for (auto const &val:f)
	if (gsl_vector_get (sol, val) < -1E-12)
	  h1[val] = 1;
      h2.reset ();
    for (auto const &val:nonf)
	if (gsl_vector_get (grad, val) < -1E-12)
	  h2[val] = 1;

      notgood = h1.count () + h2.count ();

      if (notgood < t)
	{
	  t = notgood;
	  p = 3;
	  h1c = h1;
	  h2c = h2;
	}
      if (notgood >= t && p >= 1)
	{
	  p--;
	  h1c = h1;
	  h2c = h2;
	}
      if (notgood >= t && p == 0)
	{
	  uint rh1 = 0, rh2 = 0;
	  for (uint i = 0; i < n; ++i)
	    {
	      if (h1[i])
		rh1 = i;
	      if (h2[i])
		rh2 = i;
	    }
	  uint r = std::max (rh1, rh2);
	  if (h1[r])
	    {
	      h1c.reset ();
	      h1c[r] = 1;
	      h2c.reset ();
	    }
	  else
	    {
	      h1c.reset ();
	      h2c.reset ();
	      h2c[r] = 1;
	    }
	}

      // Update F et ~F
      // Create bitset representing f
      boost::dynamic_bitset <> f_bitset (n);
    for (auto const &val:f)
	f_bitset[val] = 1;
      // Compute f with bitset operations
      f_bitset ^= h1c;		// xor
      f_bitset |= h2c;		// or
      // Update corresponding vectors
      f.clear ();
      nonf.clear ();
      for (uint i = 0; i < n; ++i)
	{
	  if (f_bitset[i])
	    f.push_back (i);
	  else
	    nonf.push_back (i);
	}
      // Free mem
      gsl_matrix_free (ata_f);
      gsl_vector_free (atb_f);
      gsl_vector_free (sol_f);
      gsl_matrix_free (ata_nf);
      gsl_vector_free (atb_nf);
      gsl_vector_free (grad_nf);
      gsl_matrix_free (cov);
    }
  while (notgood > 0 && it < 100); // This it<1000 is a quick and dirty fix to avoid infinite loop when some columns of A are too similar. TODO do it better, eg by testing if H1/H2 keep having the same values.

  // Put final solution in x
  gsl_vector_memcpy (x, sol);

  // Free mem
  gsl_vector_free (sol);
  gsl_vector_free (grad);
  gsl_multifit_linear_free (workspace);
  return;
}


double
reduced_nnls (const gsl_matrix * a, const gsl_vector * b,
	      const gsl_matrix * ata, const gsl_vector * atb,
	      const std::vector<uint> nze, gsl_vector * x)
{
  // Create  the variables of the dimension-reduced problem
  gsl_matrix *ata_red = gsl_matrix_alloc (nze.size (), nze.size ());
  gsl_vector *atb_red = gsl_vector_alloc (nze.size ());
  gsl_vector *x_red = gsl_vector_alloc (nze.size ());
  uint i = 0;
for (auto const &val:nze)
    {
      uint j = 0;
      gsl_vector_set (x_red, i, gsl_vector_get (x, val));
      gsl_vector_set (atb_red, i, gsl_vector_get (atb, val));
    for (auto const &val2:nze)
	{
	  gsl_matrix_set (ata_red, i, j, gsl_matrix_get (ata, val, val2));
	  j++;
	}
      i++;
    }

  // Compute the reduced problem
  nnls (ata_red, atb_red, x_red);

  // Update x
  gsl_vector_set_zero (x);
  i = 0;
for (auto const &val:nze)
    gsl_vector_set (x, val, gsl_vector_get (x_red, i++));

  // Compute real error ||ax - b||_2
  gsl_vector *errvect = gsl_vector_alloc (b->size);
  gsl_vector_memcpy (errvect, b);
  gsl_blas_dgemv (CblasNoTrans, 1.0, a, x, -1.0, errvect);
  double err = gsl_blas_dnrm2 (errvect);

  gsl_matrix_free (ata_red);
  gsl_vector_free (atb_red);
  gsl_vector_free (x_red);
  gsl_vector_free (errvect);
  return err;
}
