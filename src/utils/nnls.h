// nnls.h

#include <vector>
#include <boost/dynamic_bitset.hpp>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_multifit.h>

// Compute NNLS with GSL matrix A'A and vector A'b as input
// Before call, x must contain the initial vector x0
// After call, x contain the solution vector
void nnls (const gsl_matrix * ata, const gsl_vector * atb, gsl_vector * x);

// Solve NNLS imposing a sparsity pattern (the list of non-zero
// values), using a classic NNLS solver, by first solving a
// dimension-reduced problem were the 0-valued elements of x and
// matching columns of a are ignored, and then build the sparse
// solution x from the solution of the reduced problem completed with
// zeros.
double reduced_nnls (const gsl_matrix * a, const gsl_vector * b,
		     const gsl_matrix * ata, const gsl_vector * atb,
		     const std::vector<uint> nze, gsl_vector * x);
