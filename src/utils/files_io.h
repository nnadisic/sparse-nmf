// files_io.h

#include <gsl/gsl_matrix.h>

gsl_matrix *read_matrix_from_txt (const char *filename);
gsl_vector *read_vector_from_txt (const char *filename);

void print_gslmat_to_txt (const gsl_matrix * m, const char *filename);
void print_gslmat (const gsl_matrix * m);
void print_gslvect (const gsl_vector * v);
