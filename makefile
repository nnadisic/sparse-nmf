# To build with -g flag (for a gdb-ready executable), add 'GDB=1' after the command 'make'
# for -pg add 'PG=1'

NAME = arborescent

CXX = g++ -Ofast
ifdef GDB
CXX += -g -Og # last -O option override previous ones
endif
ifdef PG
CXX += -pg
endif

EXTRA_CXXFLAGS = -std=c++11 -Wall -Wextra #-fopenmp
LDLIBS=`pkg-config --libs gsl` -lboost_program_options -lcblas -latlas #-fopenmp
SRC = src/utils src/main
DEPS = $(wildcard $(SRC:=/*.h)) makefile
CODE = $(wildcard $(SRC:=/*.cc))
OBJ = $(CODE:.cc=.o)

.PHONY: install clean
.SILENT: $(NAME) install

%.o: %.cc $(DEPS)
	$(CXX) -c -o $@ $< $(EXTRA_CXXFLAGS)

$(NAME): $(OBJ)
	$(CXX) -o $@ $^ $(EXTRA_CXXFLAGS) $(LDLIBS)
	echo "$(NAME) built!"

install: $(NAME)
	mv $(NAME) /usr/bin
	echo "$(NAME) installed!"

clean:
	rm -f $(patsubst %,%/*.o,$(SRC)) $(patsubst %,%/*.out,$(SRC)) $(patsubst %,%/*~,$(SRC)) $(NAME)
