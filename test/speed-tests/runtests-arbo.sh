#!/bin/bash
# use: ./runtests.sh datadir1 datadir2 ... datadirn
# where datadir* are the directories containing the datasets

EXEC="../../arborescent"
TIMEOUT="1h"

DATADIRS=$@
RESFILE="$(eval date -u +'%y-%m-%d-%H-%M-%S.res')"

# echo $DATADIRS
# echo $RESFILE
# echo -e "m\tn\tr\tk\tt min\tt max\tt med\tt mean\tt stdev\t" > $RESFILE
echo -e "m\tn\tr\tk\tavg time" > $RESFILE

for datadir in $DATADIRS; do
    # echo $datadir

    params="$(basename $datadir | tr '-' ' ' | tr -d 'k/')"
    set -- $params
    m=$1
    n=$2
    r=$3
    k=$4
    echo -n -e "$m\t$n\t$r\t$k\t" >> $RESFILE

    outtim=/tmp/time
    outtims=$datadir/times.res
    echo -n "" > $outtims
    datasets=$datadir/m*

    for data_m in $datasets; do
	data_u="$(dirname $data_m)"/"$(basename $data_m | sed '0,/m/s//u/')"

	echo "$data_m"

	timeout $TIMEOUT $EXEC -m $data_m -u $data_u -k $k \
		 | awk '{print $1;}' > $outtim 2> /dev/null
	ex=$?
	if [ $ex -eq 124 ]; then # timeout
    	    echo -e ">$TIMEOUT" >> $outtims
	else
    	    cat $outtim >> $outtims
	fi
    done

    cat $outtims | tr '.' ',' | datamash mean 1 | tee -a $RESFILE
    # to replace datamash line, if you need more details:
    # datamash min 1 max 1 median 1 mean 1 sstdev 1
done
