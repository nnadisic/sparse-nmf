
function generator (m, n, r, k, nbs)
  %% Create directory
  dirname = sprintf('%u-%u-%u-k%u', m, n, r, k);
  mkdir(dirname);
  cd(dirname);
  for i = 1:nbs
    %% Generate U and sparse V, and compute M
    U = rand (m, r);
    V = rand (r, n);
    for j = 1:n
      V(:,j) = full(sprand(r, 1, k/r));
    end
    M = U*V;
    %% Write txt files
    write_mat(M, sprintf('m%i.txt', i));
    write_mat(U, sprintf('u%i.txt', i));
    write_mat(V, sprintf('expected-v%i.txt', i));
    %% Write matlab matrices file
    save(sprintf('savemuv%i.mat', i), 'M', 'U', 'V', 'k', 'r');
  end
  cd('..');
end

function write_mat (mat, filename)
  file = fopen(filename, 'w');
  [m, n] = size(mat);
  fprintf(file, '%u %u\n', m, n);
  for i = 1:m
    for j = 1:n
      fprintf(file, '%u ', mat(i,j));
    end
    fprintf(file, '\n');
  end
  fclose(file);
end
