% find endmembers with Separable NMF and then do Sparse Coding/Regression
% with arborescent and others

clear; clc;

% parameters
r = 12;
k = 5;

load('cuprite_ref.mat');

% compute U with FastSepNMF
K = FastSepNMF(x,r);
U = x(:,K);

% normalize cols of U
U=U./sqrt(sum(U.^2));

% compute V with NNLS
% disp('Run NNLS'); tic;
% nnlsV = zeros(r,size(x,2));
% for i=1:size(nnlsV,2)
%    nnlsV(:,i) = lsqnonneg(U,x(:,i)); 
% end
% affichage(nnlsV',4,250,191,1);
% toc

% compute V with LASSO
disp('Run LASSO'); tic;
V0 = ones(r,size(x,2));
[lassoV,lassoErr,lassoTime,~] = sHALSaccNNLS(x',V0',U',0.2,0.5,0.1,500,50);
% Postprocess by col to keep only the k highest entries and reoptim
lassoV = lassoV';
for i=1:size(lassoV,2)
    [~,ixn] = maxk(lassoV(:,i),k);
    tmp = zeros(r,1);
    tmp(ixn) = lsqnonneg(U(:,ixn),x(:,i));
    lassoV(:,i) = tmp;
end
affichage(lassoV',4,Lines,Columns,1);
toc

% compute V with arborescent
% disp('Run arborescent'); tic;
% [arboV, arboErr, arboTime] = arborescentMex(x,U,k);
% affichage(arboV',4,Lines,Columns,1);
% toc

% Reconstruction errors
[norm(x-U*nnlsV,'fro') norm(x-U*lassoV,'fro') norm(x-U*arboV,'fro')]
% Sparsity percentage
1 - [numel(nnlsV(nnlsV<1e-6))/numel(nnlsV) numel(lassoV(lassoV<1e-6))/numel(lassoV) numel(arboV(arboV<1e-6))/numel(arboV)]
% Relative error ||X - X(:,K) H ||_F / ||X||_F in percentage
[norm(x-U*nnlsV,'fro') norm(x-U*lassoV,'fro') norm(x-U*arboV,'fro')]./norm(x,'fro').*100

