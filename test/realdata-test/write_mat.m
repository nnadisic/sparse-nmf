function write_mat (mat, filename)
  file = fopen(filename, 'w');
  [m, n] = size(mat);
  fprintf(file, '%u %u\n', m, n);
  for i = 1:m
    for j = 1:n
      fprintf(file, '%u ', mat(i,j));
    end
    fprintf(file, '\n');
  end
  fclose(file);
end
