clc; load cbclim;
% Generating random initial iterates
[m,n] = size(M); r = 49;
U0 = rand(m,r); V0 = rand(r,n);
maxiter = 1e6; timelimit = 5;
[Uha,Vha,eha,tha] = HALSacc(M,U0,V0,0.5,0.1,maxiter,timelimit);
affichage(Uha,7,19,19);
U = Uha;
H = U';
for i = 1 : 361
Hn(:,i) = H(:,i)/max(H(:,i));
end
hist( sum( H > 0.01 ) );
affichage(Uha,7,19,19);
affichage(M(:,1:25:end),10,19,19);
