% given Y (224*47500) and M (224*12) find X (12*47500)
% such that Y ~= M * X

clear all; clc;


% Sparsity parameter
k = 5;

load('groundTruth_Cuprite_nEnd12.mat');
load('CupriteS1_F224.mat');

disp('Running arborescent');
[arbox,arboerr,arbotime,arbonne] = arborescentMex(Y,M,k);
resfilename = sprintf('results-k%u', k);
save(resfilename,'arbox','arboerr','arbotime','arbonne')
affichage(arbox',4,nRow,nCol,1);
