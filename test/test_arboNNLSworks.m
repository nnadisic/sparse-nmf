% Simple test to see if arbo is working (to use during dev)

clc; clear;

% mex src/mex/arboNNLSMex.cc src/mex/mexutils.cc src/main/bab.cc src/utils/*.cc -lgsl -lcblas -latlas

m = 100;
n = 12;
k = 8;

A = rand(m,n);

x = rand(n,1);
[~,sorx] = sort(x);
x(sorx(1:n-k)) = 0;

b = A*x;

% Add noise
% noiseb = randn(m,1);
% noiseb = noiseb/norm(noiseb);
% b = b + 0.15 * noiseb * norm(b);
% b(b < 0) = 0; % nonneg

% Init x from sparseHALSacc
initx = rand(n,1);
[initx,~,~,~] = sHALSaccNNLS(b',initx',A',(n-k)/n,0.5,0.1,1000,10);
initx = initx';

[arbox,nne,~] = arboNNLSMex(A,b,k);
[arbox2,nne2,~] = arboNNLSMex(A,b,k,initx);

[x arbox arbox2]
[nne nne2]
