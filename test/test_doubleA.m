clear all; clc;
format short g;

%% Parameters

m = 100;
n = 20;
k = 8;
noisyb = 0;

%% Utility functions

% Normalize to 1 all columns of given matrix
normc = @(m)m./sqrt(sum(m.^2));


%% Data generation

A = rand(m,n);

% Create sparsity on x
x = rand(n,1);
[~,sorx] = sort(x);
x(sorx(1:n-k)) = 0;

% Repeated columns
A = [A A];
x = [x; zeros(n,1)];

% Normalize A
A(A<0)=0; % nonneg
A = normc(A);

% True b
b = A*x;

% Add noise to b
if noisyb == 1
    noiseb = randn(m,1);
    noiseb = noiseb/norm(noiseb);
    b = b + 0.05 * noiseb * norm(b);
    b(b < 0) = 0; % nonneg
end


%% Resolution
% M = UV --> M' = V' U'
%             b = A  x

% With arborescent
tic;
disp("Run arborescent");
[arbox,arboerr,arbotime,arbonne] = arborescentMex(b,A,k);
timearbo = toc;

timearbo
arbonne

n = 2*n;
[x(1:n/2) x(n/2+1:end) arbox(1:n/2) arbox(n/2+1:end)]

samesupport = @(x1,x2) min(xor(logical(x1(1:size(x1)/2)),logical(x1(size(x1)/2+1:end))) == xor(logical(x2(1:size(x1)/2)),logical(x2(size(x1)/2+1:end))));
samesupport(x,arbox)


% end of file