% Simple test to see if arbo is working (to use during dev)

clc; clear all;

% mex src/mex/arborescentMex.cc src/mex/mexutils.cc src/main/snmf.cc src/main/bab.cc src/utils/*.cc -lgsl -lcblas -latlas

m = 50;
n = 100;
r = 10;
k = 6;

% Generate U
U = rand(m,r);

% Generate V col-wise k-sparse
V = zeros(r,n);
for i = 1 : n
    % Generate k-sparse column
    col = rand(r,1);
    [~,sorcol] = sort(col);
    col(sorcol(1:r-k)) = 0;
    % Normalize
    V(:,i) = V(:,i)/norm(V(:,i));
    % Copy to H
    V(:,i) = col;
end

% Compute M
M = U * V;

% Initial V from sparseHALSacc
initV = rand(r,n);
[initV,~,~,~] = sHALSaccNNLS(M',initV',U',(r-k)/r,0.5,0.1,1000,10);
initV = initV';

[arboV,arboerr,arbotime,arbonne] = arborescentMex(M,U,k);
[arboV2,arboerr2,arbotime2,arbonne2] = arborescentMex(M,U,k,initV);

% V
% arboV
% arboV2
% [arboerr arboerr2]
% [arbotime arbotime2]
% [arbonne arbonne2]
[sum(arbonne) sum(arbonne2)]