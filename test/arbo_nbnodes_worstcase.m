
function nnewc=arbo_nbnodes_worstcase(n,k)
% Compute the number of nodes arbo will explore in worst case
nnewc = 0;
for l=k:n
    nnewc = nnewc + nchoosek(n,l);
end
end