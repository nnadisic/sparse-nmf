% Sparse NNLS test framework

% TODO need to fix arbo bug for doubleA

clear all; clc;
format short g;

%% Parameters

n = 20;
k = 10;
doubleA = 0;

nbRuns = 100;

%% Utility functions

% Normalize to 1 all columns of given matrix
normc = @(m)m./sqrt(sum(m.^2));

% Return true if x1 and x2 have the same support
if doubleA == 1
    samesupport = @(x1,x2) min(xor(logical(x1(1:size(x1)/2)),logical(x1(size(x1)/2+1:end))) == xor(logical(x2(1:size(x1)/2)),logical(x2(size(x1)/2+1:end))));
else
    samesupport = @(x1,x2) min(logical(x1) == logical(x2));
end

for m=[1000 100 20]
    for illcondA=[0 1]
        for noisyb=[0 1]

            %% Prepare input
            hals_errors = []; cvx_errors = []; nnomp_errors = []; nnols_errors = []; arbo_errors = [];
            hals_times = []; cvx_times = []; nnomp_times = []; nnols_times = []; arbo_times = [];
            hals_success = 0; cvx_success = 0; nnomp_success = 0; nnols_success = 0; arbo_success = 0;

            %% Loop
            for it = 1:nbRuns
                %fprintf('%d out of %d runs\n',it,nbRuns);

                %% Data generation

                A = rand(m,n);

                % Make A ill-conditioned
                if illcondA == 1
                    [u,~,v] = svd(A);
                    s = diag( logspace(-6,0,n) );
                    A = u(:,1:n)*s(1:n,1:n)*v(:,1:n)';
                end

                % Create sparsity on x
                x = rand(n,1);
                [~,sorx] = sort(x);
                x(sorx(1:n-k)) = 0;

                % Repeated columns
                if doubleA == 1
                    A = [A A];
                    x = [x; zeros(n,1)];
                end

                % Normalize A
                A(A<0)=0; % nonneg
                A = normc(A);

                % True b
                b = A*x;

                % Add noise to b
                if noisyb == 1
                    noiseb = randn(m,1);
                    noiseb = noiseb/norm(noiseb);
                    %noiseb(noiseb < 0) = 0; % nonneg
                    b = b + 0.05 * noiseb * norm(b);
                    b(b < 0) = 0; % nonneg
                end


                %% Resolution
                % M = UV --> M' = V' U'
                %             b = A  x

                % With sHALSacc
                tic;
                %disp("Run sHALSacc");
                x0 = ones(size(x,1),1);
                [xn,e,t,lambdaU] = sHALSaccNNLS(b',x0',A',0.5,0.5,0.1,1000,10);
                [~,ixn] = maxk(xn,k);
                xn2 = zeros(1,size(x,1));
                xn2(ixn) = lsqnonneg(A(:,ixn),b);
                xn2 = xn2';
                xn = xn';
                timehals = toc;

                % With cvx
                tic;
                %disp("Run cvx");
                xcvx = NNLScvx(A,b,lambdaU);
                [~,icvx] = maxk(xcvx,k);
                xcvx2 = zeros(1,size(x,1));
                xcvx2(icvx) = lsqnonneg(A(:,icvx),b);
                xcvx = xcvx2';
                timecvx = toc;

                % With nnomp
                tic;
                %disp("Run nnomp");
                [nnompx,nnompS,nnomperr] = nnomp(b,A,k);
                nnompx = full(nnompx);
                timennomp = toc;

                % With nnols
                tic;
                %disp("Run nnols");
                [nnolsx,nnolsS,nnolserr] = nnols(b,A,k);
                nnolsx = full(nnolsx);
                timennols = toc;

                % With arborescent
                tic;
                %disp("Run arborescent");
                %[arbox,arboerr,arbotime,arbonne] = arborescentMex(b,A,k);
                [arbox,~,~] = arboNNLSMex(A,b,k);
                timearbo = toc;


                %% Store results
                hals_errors = [hals_errors norm(A*xn2-b)/norm(b)];
                cvx_errors = [cvx_errors norm(A*xcvx-b)/norm(b)];
                nnomp_errors = [nnomp_errors norm(A*nnompx-b)/norm(b)];
                nnols_errors = [nnols_errors norm(A*nnolsx-b)/norm(b)];
                arbo_errors = [arbo_errors norm(A*arbox-b)/norm(b)];

                hals_times = [hals_times timehals];
                cvx_times = [cvx_times timecvx];
                nnomp_times = [nnomp_times timennomp];
                nnols_times = [nnols_times timennols];
                arbo_times = [arbo_times timearbo];

                hals_success = hals_success + samesupport(x,xn2);
                cvx_success = cvx_success + samesupport(x,xcvx);
                nnomp_success = nnomp_success + samesupport(x,nnompx);
                nnols_success = nnols_success + samesupport(x,nnolsx);
                arbo_success = arbo_success + samesupport(x,arbox);

            end

            %% Output
            % [mean(hals_errors) mean(cvx_errors) mean(nnomp_errors) mean(nnols_errors) mean(arbo_errors)]
            % [mean(hals_times) mean(cvx_times) mean(nnomp_times) mean(nnols_times) mean(arbo_times)]
            % [hals_success cvx_success nnomp_success nnols_success arbo_success]

            fprintf('m %d illcond %d noisy %d',m, illcondA, noisyb);
            [mean(hals_errors)*100 mean(hals_times)*1e3 hals_success
                mean(cvx_errors)*100 mean(cvx_times)*1e3 cvx_success
                mean(nnomp_errors)*100 mean(nnomp_times)*1e3 nnomp_success
                mean(nnols_errors)*100 mean(nnols_times)*1e3 nnols_success
                mean(arbo_errors)*100 mean(arbo_times)*1e3 arbo_success]

        end
    end
end
%% Print results of last run
% disp("Print results");
%
% if doubleA == 1
%     [x(1:n/2) x(n/2+1:end) xn2(1:n/2) xn2(n/2+1:end)...
%         xcvx(1:n/2) xcvx(n/2+1:end) nnompx(1:n/2) nnompx(n/2+1:end)...
%         nnolsx(1:n/2) nnolsx(n/2+1:end) arbox(1:n/2) arbox(n/2+1:end)]
% else
%     [x xn2 xcvx nnompx nnolsx arbox]
% end
%
% [norm(A*xn2-b) norm(A*xcvx-b) norm(A*nnompx-b) norm(A*nnolsx-b)...
%     norm(A*arbox-b)]
%
% [samesupport(x,xn2) samesupport(x,xcvx)...
% samesupport(x,nnompx) samesupport(x,nnolsx) samesupport(x,arbox)]
%
% [timehals timecvx timennomp timennols timearbo]


% end of file