% test script to see arborescent limits, in running time and pruning, 
% when n is increasing

clear all; clc;
format short g;

%% Params
nbRuns = 10;
m = 100;

%% Prepare outputs
nvalues = [];
times = [];
nnes = [];
brutennes = [];

%% Main loop
for n=10:2:30
    k = n/2;
    timesit = [];
    nnesit = [];
    
    for it=1:nbRuns
       fprintf('n = %d it %d out of %d\n', n, it, nbRuns); 
       
       %% Generate random data
       A = rand(m,n);
       x = rand(n,1);
       [~,sorx] = sort(x);
       x(sorx(1:n-k)) = 0;
       b = A*x;
       
       % Add noise to b
       noiseb = randn(m,1);
       noiseb = noiseb/norm(noiseb);
       b = b + 0.05 * noiseb * norm(b);
       b(b < 0) = 0; % nonneg
       
       %% Compute
       tic;
       [~,~,~,arbonne] = arborescentMex(b,A,k);
       arbotime = toc;
       
       %% It Output
       timesit = [timesit arbotime];
       nnesit = [nnesit arbonne];
       
    end
    
    %% Avg output
    nvalues = [nvalues n];
    times = [times mean(timesit)];
    nnes = [nnes mean(nnesit)];
    brutennes = [brutennes nchoosek(n, k)];
end

[nvalues ; times ; nnes ; brutennes]

scatter(nvalues,nnes,'filled'); hold on; scatter(nvalues,brutennes,'filled');set(gca,'yscale','log'); legend('arbo','bruteforce');
