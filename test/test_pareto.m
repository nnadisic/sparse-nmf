% Test the "Pareto front" function of arbo

clc; clear;
 
% mex src/mex/arboNNLSMex.cc src/mex/mexutils.cc src/main/bab.cc src/utils/*.cc -lgsl -lcblas -latlas

m = 100;
n = 12;
realk = 8;
targetk = 1;

A = rand(m,n);

x = rand(n,1);
[~,sorx] = sort(x);
x(sorx(1:n-realk)) = 0;

b = A*x;

% Add noise
noiseb = randn(m,1);
noiseb = noiseb/norm(noiseb);
b = b + 0.15 * noiseb * norm(b);
b(b < 0) = 0; % nonneg

[arbox,arbonne,arbosols] = arboNNLSMex(A,b,targetk);

nbnodes = 0; 
kprime_errors = [];
it = 0;
for i=targetk:n
    it = it +1;
    nbnodes = nbnodes + nchoosek(n,i);
    kprime_errors = [kprime_errors norm(A*arbosols(:,it)-b)]; 
end

fprintf('%d nodes explored out of %d', arbonne, nbnodes);
% [x arbox]
arbosols
kprime_errors
arbonne

scatter(targetk:n,fliplr(kprime_errors),'filled'); hold on;
xlabel('k'); ylabel('Reconstr. error'); hold off;
