% Bisection with CVX

function [x,lambda] = sparseNNLScvx(A,b,k,maxiter)

n = size(A,2); 

[xn,e,t,lambdaU] = sHALSaccNNLS(b',ones(1,n),A',1-k/n,0.5,0.1,1000,10);
lambda = lambdaU; 

x = NNLScvx(A,b,lambda); 

i = 1; 
while (sum(x > 0) > k || sum(x > 0) < k) && i <= maxiter
    
    if sum(x > 0) > k
        lambda = lambda*1.01;
    else
        lambda = lambda*0.99;
    end
    
    x = NNLScvx(A,b,lambda); 
    
    i = i+1; 
end