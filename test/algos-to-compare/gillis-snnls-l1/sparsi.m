% Compute "sparsity" of matrix U : number of elements smaller than 10^-6 of
% the largest element of the corresponding column. 

function sU = sparsi(U)

U = abs(U); [m,r] = size(U); 
d = max(U); U = U*diag(d.^-1); 
sU = 100*sum(U(:)<1e-6)/m/r; 