% Solve NNLS

function x = NNLScvx(A,b,lambda)

[m,n] = size(A); 
% Create and solve the model
cvx_begin quiet
    % Variables 
    variable x(n)
    variable t
    % Objectif
    minimize( t^2 + lambda * norm(x,1) )
    % Contraintes
    subject to
        t >= norm(A*x-b,2); 
        x >= 0; 
cvx_end