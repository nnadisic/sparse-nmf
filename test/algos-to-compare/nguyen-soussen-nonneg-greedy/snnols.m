% function [x,S,err2] = snnols(y,H,K,varargin)
%
% This program is a supplementary material to the paper:
%
% Non-Negative Orthogonal Greedy algorithms, by T.T. Nguyen,
% J. Idier, C. Soussen, and E.-H. Djermoune. 
%
% https://hal.archives-ouvertes.fr/hal-02049424
%
% Minimize || y-Hx ||_2^2 s.t. ||x||_0 <= K, x>=0
%
% using Suboptimal Non-negative Orthogonal Least Squares 
% [Yaghoobi et al, 2015] integrating Support Shrinkage.
%
% Fast implementation using Active set-NNLS [Lawson et al, 1974] with warm start
%
% INPUTS
%
% y: data signal, vector of size mx1
% H: normalized dictionary, matrix of size mxn
% K: desired sparsity level
% 
% OPTIONAL INPUTS
%
% epsilon : additional stopping condition || y-Hx||_2 < epsilon. 
%           epsilon = 0 by default
% VERB:  =1 for verbose mode (many prints on screen), = 0 (no print) by default
%
% OUTPUTS:
%
% x: solution, vector of size nx1
% S: support of solution, vector of size 1 x k with k <= K
% err2: related squared error || y - Hx ||_2^2 (scalar)
%
% Date: Feb. 5, 2019
% Last modified: Feb. 5, 2019
% Authors: T.T. Nguyen, J. Idier, C. Soussen, and E.-H. Djermoune
%----------------------------------------------------------------------
% ******  Copyright (c) 2019   *******
% ******  License CeCILL       *******
%----------------------------------------------------------------------

function [x,S,err2] = snnols(y,H,K,varargin)

if nargin==3,
    epsilon = 0;
    VERB = 0;
elseif nargin==4,
    epsilon = varargin{1};
    VERB = 0;
elseif nargin==5,
    epsilon = varargin{1};
    VERB = varargin{2};
end

roundoff = eps;    % for comparisons with 0

Hy = H'*y;        
y2 = y'*y;        

% current support 
S = []; 
% full list of selected indices including those that are deselected
% during the support compression steps, with S subset Sfull due to 
% support compression
Sfull = [];   

Hsfull_H = [];   % matrix H(:,Sfull)'*H 
ind_sel = [];    % location of the elements of S in Sfull

Sall = 1:size(H,2);   % all atoms

xs = [];         % coefficients of selected atoms
theta = [];      % inverse of the Gram matrix H(:,S)'*H(:,S)
err2 = y2;       % squared error

iter = 0;

if VERB==1
    fprintf('\n\nIter.\t MOVE \t MSE\n');
    fprintf('-------------------------------\n');
    fprintf('%d:\t\t%1.5e\n',iter,err2);
end


while length(S)< K && (err2-epsilon^2)>roundoff
    
    iter = iter+1;
    
    %------------------------------------------------------------------
    % 1. Support augmentation
    %------------------------------------------------------------------
    if isempty(S) % first iteration
        prod = Hy;
        descend = prod>0;     % 1 for descending atoms 
        prod = prod(descend); % keep only the descending atoms
        
        if isempty(prod),     % no descending atom
            break;
        end;

        % Type-I pruning: remove non-descending candidate atoms
        Sbar = Sall(descend);

        theta = 1;
        % Compute maximum decrease of squared error for all descending atoms:
        [dE,idx] = max(prod);
        err2 = err2-dE*dE;      % Update of squared error
        l = Sbar(idx);          % selected atom
        xs = prod(idx);         % coefficient
        
        % Update Sfull, S, Hsfull_H
        S = l;
        Sfull = l; 
        Hsfull_H = H(:,l)'*H;
        ind_sel = 1;                % location of the elements of S in Sfull
        lst_add_rm = [];

    else % from second iteration
        Sbar = setdiff(Sall,S);

        % Check descending atoms by computing the inner products with residual
        prod = Hy(Sbar)-Hsfull_H(ind_sel,Sbar)'*xs;
        
        descend = prod>0;     % 1 for descending atoms 
        prod = prod(descend); % keep only the descending atoms
        
        if isempty(prod),  % no descending atom
            break;
        end;
        
        % Type-I pruning: remove non-descending candidate atoms
        Sbar = Sbar(descend);

        % Compute in parallel the squared residual errors related to the 
        % insertion of atoms for all candidate atoms
        phi_vect = Hsfull_H(ind_sel,Sbar);         % matrix  k x (n-k)
        thetaphi_vect = theta*phi_vect;            % matrix k x (n-k)

        % vector size (n-k):
        delta_vect = 1./(1 - sum(phi_vect.*thetaphi_vect,1)'); 
        nu_vect = phi_vect'*xs-Hy(Sbar);  % vector (n-k)
        err2_vect = err2 - delta_vect.*nu_vect.*nu_vect;  % vector (n-k)
        
        %---------   ATOM SELECTION  ----------------------------------------
        [err2_uls,idx] = min(err2_vect);
        l = Sbar(idx);   % selected atom
        %--------------------------------------------------------------------
        
        % Update S 
        S = [S, l];      % augmented support S U {l}
        
        % Updaye Sfull, Hsfull, ind_sel
        [reselection, i] = ismember(l,Sfull);
        if (reselection),    
            % reselection of a previously deleted atom
            % inner product H(:,l)'*H is already stored in row i of Hsfull_H
            
            % Move row number i of Hsfull_H to the last row of Hsfull_H
            tmp = Hsfull_H(i,:);  
            Hsfull_H(i,:)=[];
            Hsfull_H = [Hsfull_H; tmp];   

            % Same for Sfull 
            Sfull(i) = [];
            Sfull = [Sfull, l];
            
            % Update the location of the elements of S in Sfull (recall
            % that S is a subset of Sfull)
            [~, ind_sel] = ismember(S,Sfull);        
        else,     
            % new selection
            Sfull = [Sfull, l];
            % Add one new row to H(:,Sfull)'*H corresponding to the selected atom:
            Hsfull_H = [Hsfull_H; H(:,l)'*H];   

            % Update the location of the elements of S in Sfull
            ind_sel = [ind_sel,length(Sfull)];
        end;

        
        %------------------------------------------------------------------
        % 2. Compute ULS solution
        %------------------------------------------------------------------
        x_uls = [xs; 0]+ ...
                delta_vect(idx)*nu_vect(idx)*[thetaphi_vect(:,idx);-1];
        
        % Update Gram matrix for subset S U {l}
        theta = [theta,zeros(length(S)-1,1); zeros(1,length(S))] +...
                delta_vect(idx)*[thetaphi_vect(:,idx);-1]*[thetaphi_vect(:,idx)',-1];
        
        lst_add_rm = [];
        
        %------------------------------------------------------------------
        % 3. Compute NNLS solution
        %------------------------------------------------------------------
        if (min(x_uls)<0),
            % The new support is not positive. 
            %
            % Call NNLS (S UNION {l}) knowing the NNLS solution xs (S)
            % and the ULS solution x_uls (S UNION {l})
            [xs,V,theta,err2,lst_add_rm]...
                = AS_NNLS(Hy(S),Hsfull_H(ind_sel,S),xs,theta,err2_uls,VERB,x_uls);
            if (VERB),
                S_save = S;        % Save for printing support changes
            end;
            
            %------------------------------------------------------------------
            % 4. Support compression
            %------------------------------------------------------------------
            S = S(V);                % support compression
            ind_sel = ind_sel(V);    % update of list of indices of S in Sfull
        else,      
            % NNLS solution = ULS solution
            xs = x_uls;
            err2 = err2_uls;
        end
    end
     
    % print out support changes
    if (VERB==1)
        fprintf('-------------------------------\n');
        if (isempty(lst_add_rm)),
            fprintf('%d:\t+%d\t%1.5e\n',iter,l,err2);
        else,
            fprintf('%d:\t+%d\t\n',iter,l);
            for i=1:(length(lst_add_rm)-1)
                if (lst_add_rm(i)>0)
                    fprintf('\t+%d\t\n',S_save(lst_add_rm(i)));
                else % lst_add_rm(i) < 0
                    fprintf('\t-%d\t\n',S_save(-lst_add_rm(i)));
                end
            end
            i=length(lst_add_rm);
            if (lst_add_rm(i)>0)
                fprintf('\t+%d\t%1.5e\n',S_save(lst_add_rm(i)),err2);
            else, 
                % Case lst_add_rm(i) < 0:
                fprintf('\t-%d\t%1.5e\n',S_save(-lst_add_rm(i)),err2);
            end
        end
    end
end

x =spalloc(size(H,2),1,nnz(xs)); x(S) = xs;

if VERB==1
    fprintf('-------------------------------\n\n');
end

