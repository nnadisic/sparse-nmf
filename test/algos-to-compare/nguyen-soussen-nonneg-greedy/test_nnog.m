% This program is a supplementary material to the paper:
%
% Non-Negative Orthogonal Greedy algorithms, by T.T. Nguyen, J. Idier, 
% C. Soussen, and E.-H. Djermoune. 
%
% https://hal.archives-ouvertes.fr/hal-02049424
% 
% Restoration of 15 Gaussian features from noisy data using NNOG algorithms
%
%	The program displays a figure showing:
%	- the locations of the true spikes and the spikes estimated by the
%	  NNOG algorithm
%	- the data signal together with its approximation
%
%  The default setting (ALGO=1) calls nnomp.m
%
%  Set ALGO = 2 for calling snnols.m
%  Set ALGO = 3 for calling nnols.m
%
%  VERB parameter is set to 1 for many prints on screen, 0 otherwise
%
% Date: Feb. 5, 2019
% Last modified: Feb. 5, 2019
% Authors: T.T. Nguyen, J. Idier, C. Soussen, and E.-H. Djermoune
%----------------------------------------------------------------------
% ******  Copyright (c) 2019   *******
% ******  License CeCILL       *******
%----------------------------------------------------------------------


clear all;
close all;
clc;
warning off;

randn('seed',11);
rand('seed',11);

ALGO = 0;      % 1 = NNOMP - 2 = SNNOLS, 3 = NNOLS
M = 300;       % size of observation 
sigma = 7;     % standard deviation of Gaussian features
NB_GAUSS = 15; % number of Gaussian features 
SNR = 30;      % signal-to-noise ratio
VERB = 1;      % = 1 for many prints on screen, 0 otherwise

%-------------------------------------------------
%----- Generation of impulse response 
%-------------------------------------------------
% Generation of a centered truncated Gaussian of support [1-M:M-1]
val_x = (1-M):(M-1);
% indices of samples at which the Gaussian is non zero
support = find(abs(val_x)<=3*sigma);  
% impulse response  
h = exp(-val_x(support).^2/(2*sigma*sigma));  % followed by zeros...
% L2-normalization
factor = norm(h,2);  
h = h/factor;

ind_start = ceil(1+3*sigma);  % centre of the first possible Gaussian
ind_end = M-ind_start+1;      % centre of the last possible Gaussian

disp('BUILDING OBSERVATION MATRIX H... (may take a few seconds)'); 
%------------------- Construction of matrix H, sparse -----
P = length(h);
N = ind_end-ind_start+1;  % size of x, or N=M-P+1, with P=length(h)
col = zeros(M,1);
col(1:P) = h';
lin = zeros(N,1);
lin(1) = h(1);
H = sptoeplitz(col,lin);  % sparse matrix

%----------------------------------------------
%----- Generation of x (sparse)    
%----------------------------------------------

xtrue = zeros(N,1);    % true spikes

% random support
indtrue = randperm(N,NB_GAUSS);

xtrue(indtrue) = abs(randn(NB_GAUSS,1)); % Non-negative ground truth

fprintf('There are %d Gaussian features = %.2g %% of the number of samples\n',...
              NB_GAUSS,NB_GAUSS/N*100);


%----------------------------------------------
%----- Generation of noisy data h*x+n 
%----------------------------------------------
y = H*xtrue;                             % noiseless data
sigmab2 = 10^(-SNR/10)*mean(y(:).*y(:)); % noise variance
sigmab = sqrt(sigmab2);
z = y + randn(size(y))*sigmab;           % noisy data

%----------------------------------------------
%----- Run NNOG algorithms
%----------------------------------------------
if (ALGO==1),
    [x,S,err2] = nnomp(z,H,NB_GAUSS,0,VERB);
elseif (ALGO==2),
    [x,S,err2] = snnols(z,H,NB_GAUSS,0,VERB); 
else,
    [x,S,err2] = nnols(z,H,NB_GAUSS,0,VERB);
end; 

%----------------------------------------------
%----- Display figure with sparse reconstruction (x) and signal
%----- approximation (y) 
%----------------------------------------------
y_rec = H*full(x);

[maxval,pos] = max(H(:,S)); ampl = x(S)'.*maxval;

[maxval,pos_true] = max(H(:,indtrue));ampl_true = xtrue(indtrue)'.*maxval;

figure(1); clf;
plot(z,'-k','linewidth',2);hold on; 
plot(y_rec,'-r','linewidth',2); 
stem(pos_true,ampl_true,'om','linewidth',2,'markersize',10,'markerface','m')
stem(pos,ampl,'ob','linewidth',2,'markersize',10);
hold off
grid on
legend('y','Hx','x*','x');
set(legend,'FontSize',20)
set(gca,'FontSize',16)


if (ALGO==1),
    title('NNOMP approximation and recovery');
elseif (ALGO==2),
    title('SNNOLS approximation and recovery');
elseif (ALGO==3),
    title('NNOLS approximation and recovery');
end;

print('../results/fig1', '-dpdf')
