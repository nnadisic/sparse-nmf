% function [x,S,err2] = nnomp(y,H,K,varargin)
%
% This program is a supplementary material to the paper:
%
% Non-Negative Orthogonal Greedy algorithms, by T.T. Nguyen, J. Idier, 
% C. Soussen, and E.-H. Djermoune. 
%
% https://hal.archives-ouvertes.fr/hal-02049424
%
% Minimize || y-Hx ||_2^2 s.t. ||x||_0 <= K, x>=0
%
% using Non-negative Orthogonal Matching Pursuit 
% [Bruckstein et al, 2008] integrating Support Shrinkage.
%
% Fast implementation using Active set-NNLS [Lawson et al, 1974] with warm start
%
% INPUTS
%
% y: data signal, vector of size mx1
% H: normalized dictionary, matrix of size mxn
% K: desired sparsity level
% 
% OPTIONAL INPUTS:
%
% epsilon : additional stopping condition || y-Hx||_2 < epsilon. 
%           epsilon = 0 by default
% VERB:  =1 for verbose mode (many prints on screen), = 0 (no print) by default
%
% OUTPUTS:
%
% x: solution, vector of size nx1
% S: support of solution, vector of size 1 x k with k <= K
% err2: related squared error || y - Hx ||_2^2 (scalar)
%
% Date: Feb. 5, 2019
% Last modified: Feb. 5, 2019
% Authors: T.T. Nguyen, J. Idier, C. Soussen, and E.-H. Djermoune
%----------------------------------------------------------------------
% ******  Copyright (c) 2019   *******
% ******  License CeCILL       *******
%----------------------------------------------------------------------

function [x,S,err2] = nnomp(y,H,K,varargin)
    
if nargin==3,
    epsilon = 0;
    VERB = 0;
elseif nargin==4,
    epsilon = varargin{1};
    VERB=0;
elseif nargin==5,
    epsilon = varargin{1};
    VERB =varargin{2};
end

roundoff = eps;    % for comparisons with 0

Hy = H'*y;
y2 = y'*y;

% current support 
S = []; 
% full list of selected indices including those that are deselected
% during the support compression steps, with S subset Sfull due to 
% support compression
Sfull = [];   

Hsfull_H = [];   % matrix H(:,Sfull)'*H 
ind_sel = [];    % location of the elements of S in Sfull

Sall = 1:size(H,2);   % all atoms

xs = [];         % coefficients of selected atoms
theta = [];      % inverse of the Gram matrix H(:,S)'*H(:,S)
err2 = y2;       % squared error

iter = 0;
[maxprod,l] = max(Hy);   % selected atom at 1st iteration


if VERB==1
    fprintf('\n\nIter.\t MOVE \t MSE\n');
    fprintf('-------------------------------\n');
    fprintf('%d:\t\t%1.5e\n',iter,err2);
end


while length(S)<K && (err2-epsilon^2)>roundoff && maxprod>roundoff
    
    iter = iter+1;

    %------------------------------------------------------------------
    % 1. Support augmentation
    %------------------------------------------------------------------
    
    % Update S
    S = [S, l];             % augmented support S U {l}
    
    % Update matrix Hsfull_H
    [reselection, i] = ismember(l,Sfull);
    if (reselection),    
        % reselection of a previously deleted atom
        % inner product H(:,l)'*H already stored in line idx of Hsfull_H

        % Move row number idx of Hsfull_H to the last row of Hsfull_H
        tmp = Hsfull_H(i,:);  
        Hsfull_H(i,:)=[];
        Hsfull_H = [Hsfull_H; tmp];   

        % Same for Sfull 
        Sfull(i) = [];
        Sfull = [Sfull, l];
        
        % Find the location of the elements of S in Sfull (recall that S is a
        % subset of Sfull)
        [~,ind_sel] = ismember(S,Sfull);        
    
    else,     
        % new selection
        Sfull = [Sfull, l];
        % Add new row to H(:,Sfull)'*H corresponding to the selected atom:
        Hsfull_H = [Hsfull_H; H(:,l)'*H];   

        % Find the location of the elements of S in Sfull (recall that S is a
        % subset of Sfull)
        ind_sel = [ind_sel, length(Sfull)];
    end;

    %-----------------------------------------------------------------------
    %---  2. call AS-NNLS for new subset S to update xs                  ---
    [xs,V,theta,err2,lst_add_rm] = AS_NNLS(Hy(S),Hsfull_H(ind_sel,S),xs,theta,err2,VERB); 
    %-----------------------------------------------------------------------

    % note: V is a subset of indices in S
    if (VERB==1),    
        % print the NNLS path with indices of selected (+) and
        % deselected (-) atoms
        fprintf('-------------------------------\n');
        if (isempty(lst_add_rm)),
            fprintf('%d:\t+%d\t%1.5e\n',iter,l,err2);
        else,
            fprintf('%d:\t+%d\t\n',iter,l);
            % Intermediate NNLS iterates:
            for i=1:(length(lst_add_rm)-1),
                if (lst_add_rm(i)>0),
                    fprintf('\t+%d\t\n',S(lst_add_rm(i)));
                else, % lst_add_rm(i) < 0
                    fprintf('\t-%d\t\n',S(-lst_add_rm(i)));
                end;
            end;
            % Final NNLS output:
            i=length(lst_add_rm);
            if (lst_add_rm(i)>0),
                fprintf('\t+%d\t%1.5e\n',S(lst_add_rm(i)),err2);
            else, 
                % Case lst_add_rm(i) < 0:
                fprintf('\t-%d\t%1.5e\n',S(-lst_add_rm(i)),err2);
            end;
        end
    end;
    
    
    %------------------------------------------------------------------
    % 3. Support compression
    %------------------------------------------------------------------
    S = S(V);              % support compression
    ind_sel = ind_sel(V);  % update of list of indices of S in Sfull
    
    % xs already updated in the call to NNLS_update, xS has same size as
    % V, and hence as the compressed support S
    
    Sbar = setdiff(Sall,S);
    % Computation of OMP criterion
    % H(:,Sbar)'r = H(:,Sbar)'y-H(:,Sbar)'*H(:,S)*x(S)
    [maxprod,idx] = max(Hy(Sbar) - Hsfull_H(ind_sel,Sbar)'*xs); 
    l = Sbar(idx); % selected atom at next iteration
end

x =spalloc(size(H,2),1,nnz(xs)); 
x(S) = xs;

if VERB==1
    fprintf('-------------------------------\n\n');
end;
