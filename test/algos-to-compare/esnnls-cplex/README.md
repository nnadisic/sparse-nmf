The code **nonneg\_optiMIP\_minL2\_constL0.m** is a modification by Nicolas Nadisic of an original code from Sébastien Bourguignon. Modifications have been made to handle a slightly different problem: find x minimizing ||Ax-b||\_2^2 such that x is entry-wise nonnegative and ||x||\_0 <= k. Also, only the over-determined case is handled.

The original code have been download from Sébastien Bourguignon's website (https://pagesperso.ls2n.fr/~bourguignon-s/download_MIP.html) in december 2019. It is associated with the following paper :
S. Bourguignon, J. Ninin, H. Carfantan and M. Mongeau, Exact sparse approximation problems via mixed-integer programming: Formulations and computational performance, IEEE Transactions on Signal Processing, 64(6):1405--1419, March 2016.
