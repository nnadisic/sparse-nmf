% Script to test S. Bourguignon's MIP program for the P_{2/0} problem,
% with Matlab and CPLEX

clear; clc;

% depends on your CPLEX installation
addpath ''/opt/ibm/ILOG/CPLEX_Studio1210/cplex/matlab/x86-64_linux/''

% simulation parameters
m = 100;
n = 10;
k = 5;

% generate A
A = rand(m,n);

% generate k-sparse x
x = rand(n,1);
[~,sorx] = sort(x);
x(sorx(1:n-k)) = 0;

% compute b
b = A*x;

% add 5% noise to b
noiseb = randn(m,1);
noiseb = noiseb/norm(noiseb);
b = b + 0.05 * noiseb * norm(b);
b(b < 0) = 0; % nonneg

% init x0
x0 = rand(size(x));

% run solver
[foundx,M,exitflag,output]  = nonneg_optiMIP_minL2_constL0(b,A,k,x0);

% display results
[x, foundx]
rel_error = norm(A*foundx-b)/norm(b)
M


