% NB: The original code is optiMIP_minL2_constL0.m from Sébastien
% Bourguignon.
% This one has been modified by Nicolas Nadisic to handle a
% slightly different problem.
% What changed:
% -Removed undertedermined case
% -Use cplexlsqnonnegmilp instead of cplexlsqmilp, to enforce
% a nonnegativity constraint.

% Minimization of the L2-norm error under an L0 norm bound (inequality) constraint


% function [x,M,exitflag,output]  = optiMIP_minL2_constL0(y,H,K,x0,M,options)
%  Inputs:
%  y is the data vector
%  H is the dictionary
%  K is the bound on the L0 norm of the unknown vector
%
%  x0 is the initial value of x. Default value: zero vector.
%  M it the initial value of parameter M required for the bigM reformulation. Default value: 1.1 max(|H'y|), where the columns of H have unit norm
%  options is a structure defining CPLEX solver options. Default values: see http://www-01.ibm.com/support/knowledgecenter/SSSA5P_12.5.1/ilog.odms.cplex.help/CPLEX/MATLAB/topics/gs_ov_toolbox.html?lang=en
%
% Outputs:
% x the solution found by the solver
% M the value for the bigM reformulation that was used, which satisfies max(|x|) < M
% exitflag is an integer identifying the reason the optimization algorithm terminated
% output is a structure containing information about the optimization

% © Sébastien Bourguignon, 12 nov. 2015

% Remark: three output variables : x,b, z added because reformulation z = Hx is
% necessary in the underdetermined case


function [x,M,exitflag,output]  = nonneg_optiMIP_minL2_constL0(y,H,K,x0,M,options)

[N,Q]=size(H);

% default parameter values
switch nargin
  case 3
    x0 = zeros(Q,1);
    M = 1.1*max(abs(H'*y));
    options = cplexoptimset;
    options.MaxTime = 1000;
  case 4
    M = 1.1*max(abs(H'*y));
    options = cplexoptimset;
    options.MaxTime = 1000;
  case 5
    options = cplexoptimset;
    options.MaxTime = 1000;
end

% NN: Original code had one section for over-determined case and one
% for the under-determined case. We consider always over-determined

Z_NQ = spdiags(zeros(N,1),0,N,Q);
C =  [H, Z_NQ];

I_Q = speye(Q);


b_in = zeros(2*Q,1);
b_in = [b_in;
        K];

A_eq = [];
b_eq = [];

ztype(1:Q) = 'C';
ztype(Q+1:2*Q)='B';

z0(1:Q,1) =  x0;
z0(Q+1:2*Q,1) = (x0~=0);


M_OK = 0;

while ~M_OK

    A_in = [I_Q,    -M*I_Q;
            -I_Q,   -M*I_Q];
    A_in = [A_in;
            zeros(1,Q), ones(1,Q)];

    [zsol,resnorm,residual,exitflag,output] = cplexlsqnonnegmilp(C,y,A_in,b_in,A_eq,b_eq,[],[],[],ztype,z0,options);

    if exitflag<0
        error(sprintf('optimization failed, exitflag = %g (probably no feasible solution)',exitflag));
    end
    x = zsol(1:Q);

    M_OK = (max(abs(x)) <= M-1e-3);

    if ~M_OK
        M = 1.1*M;
    end

end