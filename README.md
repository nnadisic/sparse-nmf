# **arborescent**

**arborescent** Realizes a Branch-and-bound Optimization to Require
  Explicit Sparseness Constraints to be Enforced on NNLS Tasks.

**arborescent** has been presented in the following article: "Exact
  Sparse Nonnegative Least Squares", by N. Nadisic et al (2020). If this
  software is useful to you, please cite the article.
  
Note that this code is still under active development. If you want to
reproduce our experiments in the exact same conditions we had when writing
the paper, please use the version of the code from 
[this commit](https://gitlab.com/nnadisic/sparse-nmf/-/tree/a5fe161966dd90ddbca138b659314c4e9da4754e).

This software solves the following problem: find the vector x (size
n), given matrix A (size m*n) and vector b (size m), such as
Ax=~b and x has a sparsity k (that is, at most k non-zero
entries). This problem, called Exact Sparse NNLS is solved with a
dedicated branch-and-bound algorithm.

It can also be used to perform this operation on matrices, that is, to
solve the following problem: find V, with provided M and U, such as
M=~UV and V has a fixed column-wise sparsity k. This problem is solved
by running our algorithm on every column on V, so by solving
M_(:i)=UV_(:i) for all i.

Optionally, an initial vector x (resp., matrix V) can be provided. If
it is not provided, the solution to the unconstrained NNLS problem is
used to initialize the algorithm. The initial vector has a significant
impact on performance, so it's better to not provide it than to
provide a bad or random one. The solution from a Sparse NNLS heuristic
is probably a good initialization.

This software can be compiled and used from the command line, like any
C/C++ program, or from Octave/Matlab using MEX.  It requires the GNU
Scientific Library (*libgsl*), the Boost library (*libboost*) and any
BLAS library (e.g. *libatlas*). It has been developed on GNU/Linux,
but should work on any OS. It is compiled as C++ but it is actually
mainly C code using a few useful structures from C++.


## Use from command line

### Compile

It can be compiled with *make* using the provided
makefile. Optionally, it can be compiled to use all CPU threads
available, by uncommenting the line before the for loop in main.cc,
and the -fopenmp flags in the makefile.

### Run

`./arborescent -m mat_m.mat -u mat_u.mat -k sparsity`

Optional : `-o output.txt` to write the resulting V matrix in plain text into a file named 'output.txt'. `-o /dev/stdout` will print it to standard output.

Optional : `-v mat_v.mat` to provide an initial matrix V.

It can be used to solve regular Sparse NNLS problems if the provided matrix M is a column vector. As this interface was intended for development only (it is easier to debug than MEX files), it does not output as much information as the MEX interface.


## Use as Octave/Matlab Executable (MEX)

### Compile

While in the root folder of the project, in Octave/Matlab, type:

For the NNLS-only version :
`mex src/mex/arboNNLSMex.cc src/mex/mexutils.cc src/main/bab.cc src/utils/*.cc -lgsl -lcblas -latlas`

For the matrix version :
`mex src/mex/arborescentMex.cc src/mex/mexutils.cc src/main/snmf.cc src/main/bab.cc src/utils/*.cc -lgsl -lcblas -latlas`

### Run

For the NNLS-only version :
`[x,nne,sols] = arboNNLSMex(A,b,k);` or
`[x,nne,sols] = arboNNLSMex(A,b,k,initx);`

For the matrix version :
`[V,err,time,nne] = arborescentMex(M,U,k);` or
`[V,err,time,nne] = arborescentMex(M,U,k,initV);`

`nne` is the number of nodes explored. It is an int for NNLS, and a vector of int for the matrix version (one entry per column of V). `sols` is a matrix, in which every column represents the solution for a constraint k', with k' taking all values between k and n.

For practical reasons, the matrix version can not output `sols` (it would be one matrix for each column).


## Test

### Quality tests

`test/test_sparseNNLS.m` generates Sparse NNLS problems (by generating randomly A and x, and computing b) and solves them using **arborescent** and various competing algorithms. Parameters concerning the dimensions and properties of input data, and the number of runs can be tuned in the script preamble. This script outputs the number of successes (finding the same support as the optimal solution), the average error, and the total running time, for each algorithm and over all the runs.

### Real data tests

`test/realdata-tests` contains various real-life data sets (CBCL, Cuprite, San Diego)
and the associated scripts to run **arborescent** and competing algorithms.

### Speed tests

The test scripts included in `speed-tests` are not used anymore. They
were intended to test **arborescent** and compare it with the
bruteforce approach.



## License

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. See https://www.gnu.org/licenses/
